const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product is required"]
    },
    description: {
        type: String,
        required: [true, "Description is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    qtyOnHand: {
        type: Number,
        required: [true, "determine qty available to sell"]
    },
    size: {
        type: String,
        required: [true, "select product size"]
    },
    variation: {
        type: String,
        required: [true, "select variation"]
    },
    price: {
        type: Number,
        required: [true, "price is required"]
    },

    userOrder: [
        {
            orderId: {
                type: String,
                required: [true, "OrderID is required"]
            },
            userId: {
                type: String,
                required: [true, "UserID is required"]
            },
            userFullName: {
                type: String,
                required: [true, "UserID is required"]
            },
            deliveryAdd: {
                type: String,
                required: [true, "Delivery address is required"]
            },
            modeOfPayment: {
                type: String,
                required: [true, "Mode of payment is required"]
            },
            purchasedOn: {
                type: Date,
                default: new Date()
            },
            orderQty: {
                type: Number,
                required: [true, "Qty ordered is required"]
            },
            size: {
                type: String,
                required: [true, "Ordered size is required"]
            },
            variation: {
                type: String,
                required: [true, "Ordered variation is required"]
            },
            price: {
                type: Number,
                required: [true, "price is required"]
            },
            totalAmount: {
                type: Number,
                default: 0
            }
        }
    ]
});

module.exports = mongoose.model("Product", productSchema);