const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    fullName: {
        type: String,
        required: [true, "Your name is required"]
    },
    email: {
        type: String,
        required: [true, "Your email is required"]
    },
    password: {
        type: String,
        required: [true, "Input a password is a must"]
    },    
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo: {
        type: String,
        required: [true, "You are required to give your contact#"]
    },    
    deliveryAdd: {
        type: String,
        required: [true, "You are required to provide your address"]
    },  
    
    inCart: [
                {
                    productId: {
                        type: String,
                        required: [true, "courseID is required"]
                    },
                    productName: {
                        type: String,
                        required: [true, "productName is required"]
                    },
                    orderQty: {
                        type: Number,
                        required: [true, "qty is required"]
                    },
                    size: {
                        type: String,
                        required: [true, "select product size"]
                    },
                    variation: {
                        type: String,
                        required: [true, "select variation"]
                    },
                    price: {
                        type: Number,
                        required: [true, "price is required"]
                    },
                    subtotal: {
                        type: Number,
                        default: 0
                    }
                }
            ],

    orderedProduct: [
                {
                    orderDetails: [    
                                {
                                    productId: {
                                        type: String,
                                        required: [true, "courseID is required"]
                                    },
                                    productName: {
                                        type: String,
                                        required: [true, "productName is required"]
                                    },
                                    orderQty: {
                                        type: Number,
                                        required: [true, "qty is required"]
                                    },
                                    size: {
                                        type: String,
                                        required: [true, "select product size"]
                                    },
                                    variation: {
                                        type: String,
                                        required: [true, "select variation"]
                                    },
                                    price: {
                                        type: Number,
                                        required: [true, "price is required"]
                                    }
                                }       
                    ],
                    totalAmount: {
                        type: Number,
                        default: 0
                    },
                    modeOfPayment: {
                        type: String,
                        required: [true, "select mode of payment"]
                    },
                    purchasedOn: {
                        type: Date,
                        default: new Date()
                    }
                }    
            ]       
});

module.exports = mongoose.model("User", userSchema);