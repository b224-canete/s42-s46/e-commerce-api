const express = require('express');
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

//Route for creating a product
router.post("/products", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
    productController.addProduct({isAdmin: userData.isAdmin}, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});


// Route for retrieving all active products
router.get("/products", (req, res) => {

    const userData = auth.decode(req.headers.authorization);

  
    productController.getAllActiveProducts().then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all  products
router.get("/allProducts", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
    productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});


// Route for retrieving all orders
router.get("/orders", auth.verify, (req ,res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin) {
        productController.getAllOrders().then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});




// WITH PARAMS

// Route for retrieving a specific product
router.get("/view/:productId", (req, res) =>{

    let userData = auth.decode(req.headers.authorization);
    
    productController.getProduct(req.params, userData).then(resultFromController => res.send(resultFromController));
});


// Route for archiving product
router.put("/:productId/archive", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
    productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});


// Route for activate product
router.put("/:productId/activate", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
    productController.activateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});


// Route for updating product information
router.put("/:productId", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});



module.exports = router;