const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");
const Product = require("../models/Product");


// Route for user registration
router.post("/register", (req, res) => {

    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user log in
router.post("/login", (req, res) => {

    userController.loginUser(req.body).then(resultFromController => res.send (resultFromController));
});


// Route for ordering
router.post("/order", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

        res.send({msg: "You're not allowed to make a purchase because you are an Admin."});

    } else {
       
        userController.order(userData, req.body).then(resultFromController => res.send(resultFromController));
    };
});


// Route for getting user's detail
router.get("/details", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
   
    userController.getUserProfile(userData).then(resultFromController => res.send (resultFromController));
});


// Stretch Goals //


// Retrieve authenticated user's orders
router.get("/orderlist", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin) {
        res.send({auth: "failed"});
    } else {
        userController.userOrderList(userData).then(resultFromController => res.send(resultFromController));
    };
});


// Route for adding to cart
router.post("/cart/add", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

        res.send({msg: "You're not allowed to add items to cart because you are an Admin."}); 
    } else {
       
        userController.addCart(userData, req.body).then(resultFromController => res.send(resultFromController));
    };
});


// Route for updating qty in the cart
router.put("/cart/update", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

        res.send({msg: "You're not allowed access to cart because you are an Admin."});

    } else {
       
        userController.changeCart(userData, req.body).then(resultFromController => res.send(resultFromController));
    };
});


// Route for removing products from cart
router.delete("/cart/remove", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);
    
    if(userData.isAdmin){

        res.send({msg: "You're not allowed access to cart because you are an Admin."});

    } else {
       
        userController.removeFromCart(userData, req.body).then(resultFromController => res.send(resultFromController));
    };
});





// With Params


// Set user as admin
router.put("/addAdmin", auth.verify, (req, res) => {

    let userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){
        userController.setUserAsAdmin(req.body).then(resultFromController => res.send(resultFromController));
    } else {
        res.send({auth: "failed"});
    };
});

// Route for proceeding order the product from cart
router.post("/cart/:cartId", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    if(userData.isAdmin){

        res.send({msg: "You're not allowed access to cart because you are an Admin."});

    } else {
       
        userController.checkOutFromCart(userData, req.params, req.body).then(resultFromController => res.send(resultFromController));
    };
});


module.exports = router;