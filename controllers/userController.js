const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// user Registration
module.exports.registerUser = (reqBody) => {

    return User.find({email: reqBody.email}).then(async result => {
        
        if(result.length > 0) {

            return {msg: "Email Exist"}; //{msg: "Sorry, email already exist."}
            
        } else {

            let newUser = new User({
                fullName: reqBody.fullName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                deliveryAdd: reqBody.deliveryAdd,
                password: bcrypt.hashSync(reqBody.password, 10)
            });

            let userCount = await User.find({}).then(result => {
                
                if(result == 0) {
                    return result;
                } else {
                    return result;
                };
            });
            
            if(userCount.length == 0) {
                newUser.isAdmin = true
            };

            return newUser.save().then((user, error) => {

                if(error) {
                    return false; //{msg: "Registration failed. Please try again"}
                } else {
                    return true; //{msg: `Hi ${reqBody.fullName}. You have successfully registered.`}
                };
            });
        };
    }); 
};



// User Log in
module.exports.loginUser = (reqBody) => {

    return User.findOne({email:reqBody.email}).then(result => {

        if(result == null){

            return false; //{msg: "Email does not exists"}

        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if(isPasswordCorrect){

                return {access: auth.createAccessToken(result)};

            } else {

                return false; //{msg: "Incorrect password. Please try again"}
            };
        };
    });
};



//Ordering
module.exports.order = async (userData, reqBody) => {

    const itemDetails = await Product.findById(reqBody.productId).then(result => {

            if(result == null ){
                return false;
            } else {
                return result;
            };
        });

    if(itemDetails.qtyOnHand == 0 || itemDetails.isActive === false) {

        return {msg: "Sorry, item is not available."};

    } else if (itemDetails.qtyOnHand < reqBody.orderQty) {

        return {msg: "Available qty is insufficient to cater the order."};

    } else {

        let isUserUpdated = await User.findById(userData.id).then(user => {

            user.orderedProduct.push({
                orderDetails: {
                    productId: reqBody.productId,
                    productName: itemDetails.name,
                    orderQty: reqBody.orderQty,
                    size: itemDetails.size,
                    variation: itemDetails.variation,
                    price: itemDetails.price
                },
                totalAmount: itemDetails.price * reqBody.orderQty,
                modeOfPayment: reqBody.modeOfPayment
            });

            return user.save().then((user, error)=> {

                if(error) {
                    return false;
                } else {
                    return true;
                };
            });
        });

        let isProductUpdated = await Product.findById(reqBody.productId).then(product => {

            product.userOrder.push({
                orderId: `${itemDetails.name}-${itemDetails.size}-${itemDetails.price}-${reqBody.orderQty}`,
                userId: userData.id,
                userFullName: userData.fullName,
                deliveryAdd: userData.deliveryAdd,
                modeOfPayment: reqBody.modeOfPayment,
                orderQty: reqBody.orderQty,
                size: itemDetails.size,
                variation: itemDetails.variation,
                price: itemDetails.price,
                totalAmount: itemDetails.price * reqBody.orderQty
            });

            product.qtyOnHand -= reqBody.orderQty;

            return product.save().then((product, error)=> {

                if(error){
                    return false;
                } else {
                    return true;
                };
            });
        });

        if(isUserUpdated && isProductUpdated) {

            return true; //{msg: "Order success!"}

        } else {

            return {msg: "Order failed. Please try again."}; //false
        };
    };
};


// User details
module.exports.getUserProfile = (userData) => {

    return User.findById(userData.id).then(result => {
            
        if(result == null) {

            return false;

        } else {
            
            result.password = "*******";
            return result;
        };
    });
};


// Stretch Goals

// Set user as admin
module.exports.setUserAsAdmin = (reqBody) => {
    
    let userAdmin = {
        isAdmin: true
    };

    return User.findByIdAndUpdate(reqBody.userId, userAdmin).then((userAdmin, error)=>{
        if(error){

            return false; //{msg: "Update failed"}

        } else {

            if(userAdmin.isAdmin) {

                return false; //{msg: "User is already an Admin."}

            } else {

                return true; //{msg: "User is succesfully added as an admin."}
            };
        };
    });
};

// Retrieve authenticated user's orders
module.exports.userOrderList = async (userData) => {
    
    let list = await User.findById(userData.id).then(result => {

        if(result == null) {

            return false;

        } else {

            return result;
        };
    });
    
    return list.orderedProduct;
};

// Add to cart
module.exports.addCart = (userData, reqBody) => {

    return User.findById(userData.id).then(async user => {

        if(user == null){

            return {msg: "Adding to cart failed. Please try again."}; //false

        } else {

            const itemDetails = await Product.findById(reqBody.productId).then(result => {

                if(result == null ){
                    return false;
                } else {
                    return result;
                };
            });
           

            const i = user.inCart.findIndex( merch => {
                return merch.productId === reqBody.productId
            });

            if( i < 0) {

                existingQty = 0 ;

            } else {

                existingQty = user.inCart[i].orderQty;
            };

            if(itemDetails.qtyOnHand == 0 || itemDetails.isActive === false) {
        
                return {msg: "Sorry, item is not available."};
        
            } else if (itemDetails.qtyOnHand < (reqBody.orderQty+existingQty)) {
        
                return {msg: "Available qty is insufficient to add to cart."};
        
            } else {
                
                if( i < 0) {

                    user.inCart.push({
                        productId: reqBody.productId,
                        productName: itemDetails.name,
                        orderQty: reqBody.orderQty,
                        size: itemDetails.size,
                        variation: itemDetails.variation,
                        price: itemDetails.price,
                        subtotal: itemDetails.price * reqBody.orderQty
                    });

                    return user.save().then((user, error)=> {

                        if(error) {
                            return {msg: "Adding to cart failed. Please try again."};
                        } else {
                            return true; //{msg: "Added to cart."}
                        }
                    })

                } else {

                    user.inCart[i].orderQty += reqBody.orderQty;
                    user.inCart[i].subtotal = user.inCart[i].orderQty * user.inCart[i].price;
        
                    return user.save().then((user, error)=> {

                        if(error) {
                            return {msg: "Adding to cart failed. Please try again."};
                        } else {
                            return true; //{msg: "Added to cart."}
                        };
                    });
                };
            };
        };
    });
};


// change qty of items in the cart
module.exports.changeCart = async (userData, reqBody) => {

    let cartUser = await User.findById(userData.id).then(result => {

        if(result == null) {
            return false;
        } else {
            return result;
        };
    });

    let cartProduct = await Product.findById(reqBody.productId).then(result => {

        if(result == null) {
            return false;
        } else {
            return result;
        };
    });
    
    const i = cartUser.inCart.findIndex( item => {
        return item.productId === reqBody.productId
    });
    
    let newQty = reqBody["qty(+add/-minus)"] + cartUser.inCart[i].orderQty;

    if(newQty < 1) {

        cartUser.inCart.splice(i,1);

        return cartUser.save().then((user, error)=> {

            if(error) {
                return false;
            } else {
                return true; //{msg: "Item remove from cart."}
            };
        });

    } else if(newQty > cartProduct.qtyOnHand) {

        return {msg: "Available qty is insufficient for the qty added to cart."};

    } else {

        cartUser.inCart[i].orderQty = newQty;
        cartUser.inCart[i].subtotal = cartUser.inCart[i].orderQty * cartUser.inCart[i].price;

        return cartUser.save().then((user, error)=> {

            if(error) {

                return false; //{msg: "Change qty failed. Please try again."}

            } else {

                return true; //{msg: "Item qty successfully updated."}
            };
        });
    };
};

// Remove products from cart
module.exports.removeFromCart = async (userData, reqBody) => {

    let cartUser = await User.findById(userData.id).then(result =>{

        if(result == null){
            return false;
        } else {
            return result;
        };
    });

    for(let x=1; x <= Object.keys(reqBody).length; x++) {

        const i = cartUser.inCart.findIndex( item => {
            return item.productId === reqBody[`productId-${x}`];
        });

        cartUser.inCart.splice(i,1);
    };

    return cartUser.save().then((user, error)=> {

        if(error) {

            return false; //{msg: "Failed to remove the product. Please try again."}

        } else {

            return true; //{msg: "Product removed from the cart."}
        };
    });
};


// Proceed check-out from Cart
module.exports.checkOutFromCart = async (userData, reqParams, reqBody) => {

    let cartUser = await User.findById(userData.id).then(result => {

        if(result == null) {
            return false;
        } else {
            return result;
        };
    });


    const i = cartUser.inCart.findIndex( item => {
        return item.id === reqParams.cartId;
    });

    let isUserUpdated = await User.findById(userData.id).then(user => {

        user.orderedProduct.push({
            orderDetails: {
                productId: cartUser.inCart[i].productId,
                productName: cartUser.inCart[i].productName,
                orderQty: cartUser.inCart[i].orderQty,
                size: cartUser.inCart[i].size,
                variation: cartUser.inCart[i].variation,
                price: cartUser.inCart[i].price
            },
            totalAmount: cartUser.inCart[i].subtotal,
            modeOfPayment: reqBody.modeOfPayment
        });

        user.inCart.splice(i,1);

        return user.save().then((user, error)=> {

            if(error) {
                return false;
            } else {
                return true;
            };
        });
    });
    
    let isProductUpdated = await Product.findById(cartUser.inCart[i].productId).then(product => {

        product.userOrder.push({
            orderId: `${cartUser.inCart[i].size}-${cartUser.inCart[i].variation}-${cartUser.inCart[i].price}-${cartUser.inCart[i].orderQty}`,
            userId: userData.id,
            userFullName: userData.fullName,
            deliveryAdd: userData.deliveryAdd,
            modeOfPayment: reqBody.modeOfPayment,
            orderQty: cartUser.inCart[i].orderQty,
            size: cartUser.inCart[i].size,
            variation: cartUser.inCart[i].variation,
            price: cartUser.inCart[i].price,
            totalAmount: cartUser.inCart[i].subtotal
        });

        product.qtyOnHand -= cartUser.inCart[i].orderQty;

        return product.save().then((product, error)=> {

            if(error){
                return false;
            } else {
                return true;
            };
        });
    });

    if(isUserUpdated && isProductUpdated) {
        
        return true; //{msg: "Successfully checked-out the product."}

    } else {

        return false; //{msg: "Check-out failed. Please try again."}
    };
};