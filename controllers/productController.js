const Product = require("../models/Product");


// Creating a new product
module.exports.addProduct = (userData, reqBody) => {

    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        qtyOnHand: reqBody.qtyOnHand,
        size: reqBody.size,
        variation: reqBody.variation,
        price: reqBody.price
    });
        
    return newProduct.save().then((product, error) => {

        if(error) {
            return false;
        } else {
            return true; //product
        };
    });
};


// Retrieving all active products
module.exports.getAllActiveProducts = () => {

    return Product.find({isActive: true}).then(result => {

        if(result.length == 0) {

            return false; //{msg: "No product available."}

        } else {

            return result;
        };
    })
 };

 // Retrieving all products
module.exports.getAllProducts = (data) => {

    return Product.find({}).then(result => {

        if(result.length == 0) {

            return false; {msg: "No product added in the system"}

        } else {
            
            return result;
        };
    });
 };


// Retrieving a specific product
module.exports.getProduct = (reqParams, data) => {

    return Product.findById(reqParams.productId).then(result => {

       /* if(result == null) {
            return {msg: "Product does not exist"};
        } else if(result.isActive == false && data == null) {
            return {msg: "Product is not available"};
        } else if(result.isActive == false && data.isAdmin == false) {
            return {msg: "Product is not available"};
        } else {
            return result;
        };*/

        if(result == null) {
            return false;
        } else {
            return result;
        }
    });
};

// Update a product
module.exports.updateProduct = (reqParams, reqBody) => {

    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        qtyOnHand: reqBody.qtyOnHand,
        size: reqBody.size,
        variation: reqBody.variation,
        price: reqBody.price
    };

    return Product.findByIdAndUpdate(reqParams.productId,updatedProduct).then((updatedProduct, error) => {

        if(error) {
            return false;
        } else {
            return true;
        };
    });
    
    //  Product.findById(reqParams.productId).then(result => {
        
    //     if(result == null) {
    //         return false; //{msg: "Product is non-existent"}
    //     } else {
    //         return true; //result
    //     };
    // });
};

// Archive product
module.exports.archiveProduct = (reqParams) => {
    
    let archivedProduct = {
        isActive: false
    };

    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error)=>{

        if(error){
            return false;
        } else {
            return true; //{msg: "Success! Product has been archived."}
        };
    });
};


// Activate product
module.exports.activateProduct = (reqParams) => {
    
    let archivedProduct = {
        isActive: true
    }
    console.log("entered")
    return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error)=>{

        if(error){
            return false;
        } else {
            return true; //{msg: "Success! Product is activated."}
        };
    });
};


// Retrieving all orders
module.exports.getAllOrders = async () => {

    let orders = await Product.find({userOrder: { $ne: [] }}).then(result =>{

        if(result == null) {
            return false;
        } else {
            return result;
        };
    });

    let newArr = [];

    for( let i=0; i < orders.length; i++) {

        newArr = newArr.concat(orders[i].userOrder);
    };
   
    return newArr;
};