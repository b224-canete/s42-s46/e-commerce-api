const jwt = require("jsonwebtoken");

const secret = "ECommerceAPI";

module.exports.createAccessToken = (user) => {
    const data = {
        id: user._id,
        fullName: user.fullName,
        email: user.email,
        deliveryAdd: user.deliveryAdd,
        isAdmin: user.isAdmin
    };

    return jwt.sign(data, secret, {}) 
};

//authentication
module.exports.verify = (req, res, next) => {

    let token = req.headers.authorization;

    if(typeof token !== "undefined") {

        console.log(token);
        token = token.slice(7, token.length)
        return jwt.verify(token,secret, (err, data) => {
            if(err) {
                return res.send({auth: "failed"})
            } else {
                next();
            }
        })
    } else {
        return res.send({auth: "failed"})
    }
};

//decode authorization
module.exports.decode = (token) => {
    if(typeof token !== "undefined") {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (err, data) => {
            if(err) {
                return null
            } else {
                return jwt.decode(token, {complete: true}.payload)
            }
        })
    } else {
        return null
    }
}